// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//.

package monitor

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

func TestIntegEntryPoint(t *testing.T) {
	testCases := []struct {
		name            string
		assetRulePath   string
		ceType          string
		wantMsgContains []string
		wantError       bool
	}{
		{
			name:            "unexpected_type",
			assetRulePath:   "testdata/assetRule_GCPBigQueryDatasetLocationConstraintV1_not_compliant.json",
			ceType:          "blabla",
			wantMsgContains: []string{"want triggering cloud event type contains com.gitlab.realtime-asset-monitor.asset_rule"},
		},
		{
			name:          "GCPBigQueryDatasetLocationConstraintV1_not_compliant",
			assetRulePath: "testdata/assetRule_GCPBigQueryDatasetLocationConstraintV1_not_compliant.json",
			wantMsgContains: []string{
				"finish not_compliant",
				"number of violations 1",
			},
		},
		{
			name:          "GCPIAMMembersConstraintV1_not_compliant",
			assetRulePath: "testdata/assetRule_GCPIAMMembersConstraintV1_not_compliant.json",
			wantMsgContains: []string{
				"finish not_compliant",
				"number of violations 10",
			},
		},
		{
			name:          "GCPIAMBindingsrolesConstraintV1_compliant",
			assetRulePath: "testdata/assetRule_GCPIAMBindingsrolesConstraintV1_compliant.json",
			wantMsgContains: []string{
				"finish compliant",
				"number of violations 0",
			},
		},
		{
			name:          "GCPBigQueryDatasetLocationConstraintV1_not_compliant_retry",
			assetRulePath: "testdata/assetRule_GCPBigQueryDatasetLocationConstraintV1_not_compliant_retry.json",
			wantMsgContains: []string{
				"retry",
			},
			wantError: true,
		},
		{
			name:          "GCPBigQueryDatasetLocationConstraintV1_not_compliant_noretry",
			assetRulePath: "testdata/assetRule_GCPBigQueryDatasetLocationConstraintV1_not_compliant_noretry.json",
			wantMsgContains: []string{
				"noretry",
			},
		},
	}
	ctx := context.Background()
	now := time.Now()

	for _, tc := range testCases {
		tc := tc // prevent G601
		t.Run(tc.name, func(t *testing.T) {
			var attr map[string]string
			var pubsubMsg pubsub.Message
			attr = make(map[string]string)
			attr["Content-Type"] = "application/json"
			attr["ce-id"] = "c56c9245-0af7-44c7-8a55-754583940e09"
			attr["ce-source"] = "fetchrules/brunore-ram-qa-100/ram/fetchrules-00035-xbc/asset_rule"
			attr["ce-specversion"] = "1.0"
			attr["ce-time"] = now.String()
			if tc.ceType != "" {
				attr["ce-type"] = tc.ceType
			} else {
				attr["ce-type"] = "com.gitlab.realtime-asset-monitor.asset_rule"
			}

			pubsubMsg.Attributes = attr
			pubsubMsg.PublishTime = now
			pubsubMsg.ID = attr["ce-id"]

			p := filepath.Clean(tc.assetRulePath)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			pubsubMsg.Data = b

			var buffer bytes.Buffer
			log.SetOutput(&buffer)
			defer func() {
				log.SetOutput(os.Stderr)
			}()
			err = EntryPoint(ctx, pubsubMsg)
			msgString := buffer.String()
			// t.Logf("%v", msgString)
			if err != nil {
				if !tc.wantError {
					t.Errorf("Want no error and got %v", err)
				}
			} else {
				if tc.wantError {
					t.Errorf("Want an error and got no error")
				}
			}

			for _, wantMsg := range tc.wantMsgContains {
				wantMsg := wantMsg
				if !strings.Contains(msgString, wantMsg) {
					t.Errorf("want msg to contains: %s", tc.wantMsgContains)
				}
			}
			logEntriesString := strings.Split(msgString, "\n")
			for _, logEntryString := range logEntriesString {
				var logEntry glo.Entry
				err = json.Unmarshal([]byte(logEntryString), &logEntry)
				if !(err != nil) {
					if logEntry.MicroserviceName == "" {
						t.Errorf("logEntry.MicroserviceName is null should not %s", logEntry.Message)
					}
					if logEntry.Environment == "" {
						t.Errorf("logEntry.Environment is null should not %s", logEntry.Message)
					}
					if strings.HasPrefix(strings.ToLower(logEntry.Message), "finish") {
						if len(logEntry.StepStack) == 0 {
							t.Errorf("logEntry.StepStack len is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencyE2ESeconds == 0 {
							t.Errorf("logEntry.LatencyE2ESeconds is zero and should not %s", logEntry.Message)
						}
						if logEntry.LatencySeconds == 0 {
							t.Errorf("logEntry.LatencySeconds is zero and should not %s", logEntry.Message)
						}
					}
				}
			}
		})
	}
}
