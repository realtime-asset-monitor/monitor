// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package monitor

import (
	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

const microserviceName = "monitor"
const triggeringEventType = "asset_rule"

// ServiceEnv list environment variables prefixed with the name of the microservice
type ServiceEnv struct {
	ComplianceStatusTopicID string `envconfig:"compliance_status_topic_id" default:"ram-complianceStatus"`
	Environment             string `envconfig:"environment" default:"dev"`
	LogOnlySeveritylevels   string `envconfig:"log_only_severity_levels" default:"WARNING NOTICE CRITICAL"`
	ProjectID               string `envconfig:"project_id" required:"true"`
	ViolationTopicID        string `envconfig:"violation_topic_id" default:"ram-violation"`
	StartProfiler           bool   `envconfig:"start_profiler" default:"false"`
}

// Env list environment variables
type Env struct {
	KConfiguration string `envconfig:"k_configuration"`
	KRevision      string `envconfig:"k_revision"`
	KService       string `envconfig:"k_service"`
}

// Global structure for global variables to optimize the performances in serverless mode
type Global struct {
	CommonEv              glo.CommonEntryValues
	complianceStatusTopic pubsub.Topic
	env                   *Env
	serviceEnv            *ServiceEnv
	violationTopic        pubsub.Topic
}

// violations array of violation
type violations []cai.Violation

type regoInput struct {
	Assets      []cai.Asset                     `json:"assets"`
	Constraints map[string]cai.ConstraintConfig `json:"constraints"`
}
