// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//..

package monitor

import (
	"context"
	"encoding/json"
	"fmt"
	"regexp"
	"strings"
	"time"

	"cloud.google.com/go/pubsub"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
	"gitlab.com/realtime-asset-monitor/utilities/cev"
	"gitlab.com/realtime-asset-monitor/utilities/glo"
)

// EntryPoint function entry point.
func EntryPoint(ctxEvent context.Context, pubsubMsg pubsub.Message) error {
	var ev glo.EntryValues
	ev.CommonEntryValues = global.CommonEv
	ceTime, err := time.Parse(time.RFC3339, pubsubMsg.Attributes["ce-time"])
	if err != nil {
		glo.LogWarning(ev, "pubsub msg publish time missing or invalid", fmt.Sprintf("time.Parse(time.RFC3339,  pubsubMsg.Attributes[\"eventTime\"]) %v", err))
	}
	ev.Step = glo.Step{
		StepID:        fmt.Sprintf("%s/%s", pubsubMsg.Attributes["ce-source"], pubsubMsg.Attributes["ce-id"]),
		StepTimestamp: ceTime,
	}
	now := time.Now()
	d := now.Sub(ev.Step.StepTimestamp)

	b, _ := json.MarshalIndent(pubsubMsg, "", "  ")
	glo.LogStartCloudEvent(ev, b, d.Seconds(), &now)

	cePrefix := fmt.Sprintf("%s.%s", cev.RAMCloudEventTypePrefix, triggeringEventType)
	if !strings.Contains(pubsubMsg.Attributes["ce-type"], cePrefix) {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("want triggering cloud event type contains %s and got %s",
			cePrefix, pubsubMsg.Attributes["ce-type"]), "", "")
		return nil
	}

	var assetRule cai.AssetRule
	err = json.Unmarshal(pubsubMsg.Data, &assetRule)
	if err != nil {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.Unmarshal(b, &assetRule) %v", err),
			assetRule.FeedMessage.Asset.AssetType,
			assetRule.Rule.Name)
		return nil
	}
	ev.StepStack = append(assetRule.StepStack, ev.Step)

	var complianceStatus cai.ComplianceStatus

	complianceStatus.AssetName = assetRule.FeedMessage.Asset.Name
	complianceStatus.AssetType = assetRule.FeedMessage.Asset.AssetType
	complianceStatus.AssetInventoryOrigin = assetRule.FeedMessage.Origin
	complianceStatus.RuleName = assetRule.Rule.Name
	complianceStatus.RuleDeploymentTimeStamp = assetRule.Rule.DeploymentTime
	complianceStatus.StepStack = ev.StepStack
	complianceStatus.EvaluationTimeStamp = time.Now()

	if complianceStatus.AssetInventoryOrigin == "real-time" {
		complianceStatus.AssetInventoryTimeStamp = assetRule.FeedMessage.Window.StartTime
	} else {
		complianceStatus.AssetInventoryTimeStamp = assetRule.FeedMessage.Asset.UpdateTime
	}

	countViolations := 0
	if assetRule.FeedMessage.Deleted {
		complianceStatus.Deleted = assetRule.FeedMessage.Deleted
		// bool cannot be nil and have a zero value to false
		complianceStatus.Compliant = true
	} else {
		complianceStatus.Deleted = false
		resultSet, err := evaluateConstraints(ctxEvent, assetRule)
		// log.Printf("\n%v\n\n", resultSet)
		if err != nil {
			if strings.Contains(err.Error(), "caller cancelled query execution") {
				glo.LogCriticalRetry(ev, fmt.Sprintf("evaluateConstraints(ctxEvent, assetRule) %v", err),
					assetRule.FeedMessage.Asset.AssetType,
					assetRule.Rule.Name)
				return err
			}
			glo.LogCriticalNoRetry(ev, fmt.Sprintf("evaluateConstraints(ctxEvent, assetRule) %v", err),
				assetRule.FeedMessage.Asset.AssetType,
				assetRule.Rule.Name)
			return nil
		}
		violations, errorType, errorDetail := inspectResultSet(resultSet)
		if errorType != "" {
			if errorType == "retry" {
				glo.LogCriticalRetry(ev, errorDetail, assetRule.FeedMessage.Asset.AssetType, assetRule.Rule.Name)
				return fmt.Errorf("transient error from rego code execution to be retried %s", errorDetail)
			}
			glo.LogCriticalNoRetry(ev, errorDetail, assetRule.FeedMessage.Asset.AssetType, assetRule.Rule.Name)
			return nil
		}
		if len(violations) == 0 {
			complianceStatus.Compliant = true
		} else {
			complianceStatus.Compliant = false
			countViolations = len(violations)
			for i, violation := range violations {
				violation := violation // prevent G601
				violation.NonCompliance.EvaluationTimeStamp = complianceStatus.EvaluationTimeStamp
				violation.FunctionConfig.ProjectID = global.serviceEnv.ProjectID
				violation.FunctionConfig.Environment = global.serviceEnv.Environment
				violation.FunctionConfig.FunctionName = assetRule.Rule.Name
				violation.FunctionConfig.DeploymentTime = assetRule.Rule.DeploymentTime
				violation.FeedMessage = assetRule.FeedMessage
				violation.RegoModules = assetRule.Rule.RegoModules
				violation.StepStack = ev.StepStack
				violationJSON, err := json.Marshal(violation)
				if err != nil {
					glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.Marshal(violation) %v", err),
						assetRule.FeedMessage.Asset.AssetType,
						assetRule.Rule.Name)
					return nil
				}
				glo.LogInfo(ev, fmt.Sprintf("not_compliant %s violationNum %d", complianceStatus.AssetName, i),
					fmt.Sprintf("origin %s timestamp %v violationJSON %s", complianceStatus.AssetInventoryOrigin, complianceStatus.AssetInventoryTimeStamp, string(violationJSON)))

				var pubSubMsg pubsub.Message
				pubSubMsg.Data = violationJSON
				pubSubMsg.Attributes = map[string]string{
					"assetType":        assetRule.FeedMessage.Asset.AssetType,
					"contentType":      assetRule.FeedMessage.ContentType,
					"messageType":      fmt.Sprintf("violation_on_rule.%s", violation.ConstraintConfig.Kind),
					"microserviceName": microserviceName,
					"origin":           assetRule.FeedMessage.Origin,
					"timestamp":        now.Format(time.RFC3339),
				}

				result := global.violationTopic.Publish(ctxEvent, &pubSubMsg)
				//block until result is returned
				id, err := result.Get(ctxEvent)
				if err != nil {
					// https://cloud.google.com/pubsub/docs/reference/error-codes
					var re = regexp.MustCompile(`500|INTERNAL|502|BAD_GATEWAY|503|UNAVAILABLE|504|DEADLINE_EXCEEDED|`)
					if re.MatchString(err.Error()) {
						glo.LogCriticalRetry(ev, fmt.Sprintf("id %s %v", id, err), assetRule.FeedMessage.Asset.AssetType, assetRule.Rule.Name)
						return err
					}
					glo.LogCriticalNoRetry(ev, fmt.Sprintf("id %s other errors %v", id, err), assetRule.FeedMessage.Asset.AssetType, assetRule.Rule.Name)
					return nil
				}
			}
		}
	}

	var status string
	if complianceStatus.Compliant {
		status = "compliant"
	} else {
		status = "not_compliant"
	}

	var pubSubMsg pubsub.Message
	pubSubMsg.Data, err = json.Marshal(&complianceStatus)
	if err != nil {
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("json.Marshal(&complianceStatus) %v", err), "", "")
		return nil
	}
	pubSubMsg.Attributes = map[string]string{
		"assetType":        assetRule.FeedMessage.Asset.AssetType,
		"contentType":      assetRule.FeedMessage.ContentType,
		"messageType":      "compliance_status",
		"microserviceName": microserviceName,
		"origin":           assetRule.FeedMessage.Origin,
		"timestamp":        now.Format(time.RFC3339),
	}

	result := global.complianceStatusTopic.Publish(ctxEvent, &pubSubMsg)
	//block until result is returned
	id, err := result.Get(ctxEvent)
	if err != nil {
		// https://cloud.google.com/pubsub/docs/reference/error-codes
		var re = regexp.MustCompile(`500|INTERNAL|502|BAD_GATEWAY|503|UNAVAILABLE|504|DEADLINE_EXCEEDED|`)
		if re.MatchString(err.Error()) {
			glo.LogCriticalRetry(ev, fmt.Sprintf("id %s %v", id, err), assetRule.FeedMessage.Asset.AssetType, assetRule.Rule.Name)
			return err
		}
		glo.LogCriticalNoRetry(ev, fmt.Sprintf("id %s other errors %v", id, err), assetRule.FeedMessage.Asset.AssetType, assetRule.Rule.Name)
		return nil
	}

	glo.LogFinish(ev,
		status,
		fmt.Sprintf("number of violations %d", countViolations),
		time.Now(),
		assetRule.FeedMessage.Origin,
		assetRule.FeedMessage.Asset.AssetType,
		assetRule.FeedMessage.ContentType,
		assetRule.Rule.Name,
		0)
	return nil
}
