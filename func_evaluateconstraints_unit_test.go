// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package monitor

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/realtime-asset-monitor/utilities/cai"
)

func TestUnitEvaluateConstraints(t *testing.T) {
	var testCases = []struct {
		name          string
		assetRulePath string
		wantErrorMsg  string
	}{
		{
			name:          "testOneViolation",
			assetRulePath: "testdata/assetRule_GCPBigQueryDatasetLocationConstraintV1_not_compliant.json",
		},
		{
			name:          "testTenViolations",
			assetRulePath: "testdata/assetRule_GCPIAMMembersConstraintV1_not_compliant.json",
		},
	}
	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			p := filepath.Clean(tc.assetRulePath)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var assetRule cai.AssetRule
			err = json.Unmarshal(b, &assetRule)
			if err != nil {
				log.Fatalln(err)
			}

			rs, err := evaluateConstraints(ctx, assetRule)
			if tc.wantErrorMsg == "" {
				if err != nil {
					t.Errorf("want no error and got %v", err)
				}
			} else {
				if !strings.Contains(err.Error(), tc.wantErrorMsg) {
					t.Errorf("want error message %s and got %s", tc.wantErrorMsg, err.Error())
				}
			}
			if len(rs) != 1 {
				t.Errorf("result set len should be 1 and is %d", len(rs))
			}
		})
	}
}
