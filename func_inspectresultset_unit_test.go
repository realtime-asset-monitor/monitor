// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package monitor

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/realtime-asset-monitor/utilities/cai"
)

func TestUnitInspectResultSet(t *testing.T) {
	var testCases = []struct {
		name               string
		assetRulePath      string
		wantViolationCount int
		wantErrorType      string
		wantErrorDetail    string
	}{
		{
			name:               "testOneViolation",
			assetRulePath:      "testdata/assetRule_GCPBigQueryDatasetLocationConstraintV1_not_compliant.json",
			wantViolationCount: 1,
		},
		{
			name:               "testTenViolations",
			assetRulePath:      "testdata/assetRule_GCPIAMMembersConstraintV1_not_compliant.json",
			wantViolationCount: 10,
		},
		{
			name:            "errorNoRetry",
			assetRulePath:   "testdata/assetRule_GCPBigQueryDatasetLocationConstraintV1_not_compliant_noretry.json",
			wantErrorType:   "noretry",
			wantErrorDetail: "400 bad request",
		},
		{
			name:            "errorRetry",
			assetRulePath:   "testdata/assetRule_GCPBigQueryDatasetLocationConstraintV1_not_compliant_retry.json",
			wantErrorType:   "retry",
			wantErrorDetail: "500 internal server error",
		},
	}
	for _, tc := range testCases {
		tc := tc // https://github.com/golang/go/wiki/CommonMistakes#using-goroutines-on-loop-iterator-variables
		t.Run(tc.name, func(t *testing.T) {
			p := filepath.Clean(tc.assetRulePath)
			if !strings.HasPrefix(p, "testdata/") {
				panic(fmt.Errorf("Unsafe path %s", p)) // prevent G304
			}
			b, err := os.ReadFile(p)
			if err != nil {
				log.Fatalln(err)
			}
			var assetRule cai.AssetRule
			err = json.Unmarshal(b, &assetRule)
			if err != nil {
				log.Fatalln(err)
			}

			rs, err := evaluateConstraints(ctx, assetRule)
			if err != nil {
				log.Fatalln(err)
			}

			violations, errorType, errorDetail := inspectResultSet(rs)

			// t.Logf("\nerr %s %s\n", errorType, errorDetail)

			if tc.wantErrorType != errorType {
				t.Errorf("want error type %s and got %s", tc.wantErrorType, errorType)
			} else {
				if tc.wantErrorDetail != errorDetail {
					t.Errorf("want error detail %s and got %s", tc.wantErrorDetail, errorDetail)
				} else {
					if len(violations) != tc.wantViolationCount {
						t.Errorf("want %d violations and got %d", tc.wantViolationCount, len(violations))
					}
					for _, v := range violations {
						if len(v.ConstraintConfig.Metadata.Annotations) == 0 {
							t.Errorf("want at least one violation constraint annotation and got none")
						}
					}
				}
			}

		})
	}
}
