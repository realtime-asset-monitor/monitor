// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package monitor

import (
	"context"
	"fmt"

	"github.com/open-policy-agent/opa/rego"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
)

func evaluateConstraints(ctx context.Context, assetRule cai.AssetRule) (resultSet rego.ResultSet, err error) {
	var regoOptions []func(*rego.Rego)
	regoOptions = append(regoOptions, rego.Query("audit"))
	regoOptions = append(regoOptions, rego.Package("validator.gcp.lib"))
	for regoFileName, content := range assetRule.Rule.RegoModules {
		regoFileName := regoFileName
		content := content
		regoOptions = append(regoOptions, rego.Module(regoFileName, content))
	}
	query, err := rego.New(regoOptions...).PrepareForEval(ctx)
	if err != nil {
		return nil, fmt.Errorf("PrepareForEval %v", err)
	}
	var data regoInput
	data.Assets = append(data.Assets, assetRule.FeedMessage.Asset)
	data.Constraints = make(map[string]cai.ConstraintConfig)
	for _, constraintConfig := range assetRule.Rule.Constraints {
		constraintConfig := constraintConfig
		data.Constraints[constraintConfig.Metadata.Name] = constraintConfig
	}
	resultSet, err = query.Eval(ctx, rego.EvalInput(data))
	if err != nil {
		return nil, fmt.Errorf("rego.EvalInput %v", err)
	}
	return resultSet, nil
}
