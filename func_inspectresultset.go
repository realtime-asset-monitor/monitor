// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.
//

package monitor

import (
	"github.com/open-policy-agent/opa/rego"
	"gitlab.com/realtime-asset-monitor/utilities/cai"
)

// inspectResultSet explore rego query output and craft violation document
func inspectResultSet(resultSet rego.ResultSet) (violations violations, errorType string, errorDetail string) {
	var violation cai.Violation
	Expressions := resultSet[0].Expressions
	if len(Expressions) != 0 {
		expressionValue := *Expressions[0]

		// log.Println("Rego Query: ", expressionValue.Text)
		// location := *expressionValue.Location
		// log.Printf("Position in query: Row %d Col %d\n", location.Row, location.Col)

		var valuesInterface interface{} = expressionValue.Value
		if values, ok := valuesInterface.([]interface{}); ok {
			// for each violation
			for i := 0; i < len(values); i++ {
				var valueInterface interface{} = values[i]
				if value, ok := valueInterface.(map[string]interface{}); ok {
					var violationInterface interface{} = value["violation"]
					if ruleViolation, ok := violationInterface.(map[string]interface{}); ok {
						var msgInterface interface{} = ruleViolation["msg"]
						if msg, ok := msgInterface.(string); ok {
							violation.NonCompliance.Message = msg
						}
						var detailsInterface interface{} = ruleViolation["details"]
						if details, ok := detailsInterface.(map[string]interface{}); ok {
							violation.NonCompliance.Metadata = details
							var eTypeInterface interface{} = details["error_type"]
							if eType, ok := eTypeInterface.(string); ok {
								errorType = eType
								var eDetailInterface interface{} = details["error_detail"]
								if eDetail, ok := eDetailInterface.(string); ok {
									errorDetail = eDetail
								}
								break
							}
						}
					}

					var constraintConfigInterface interface{} = value["constraint_config"]
					if constraintConfig, ok := constraintConfigInterface.(map[string]interface{}); ok {
						var apiVersionInterface interface{} = constraintConfig["apiVersion"]
						if apiVersion, ok := apiVersionInterface.(string); ok {
							violation.ConstraintConfig.APIVersion = apiVersion
						}
						var kindInterface interface{} = constraintConfig["kind"]
						if kind, ok := kindInterface.(string); ok {
							violation.ConstraintConfig.Kind = kind
						}
						var metadataInterface interface{} = constraintConfig["metadata"]
						if metadata, ok := metadataInterface.(map[string]interface{}); ok {
							var nameInterface interface{} = metadata["name"]
							if name, ok := nameInterface.(string); ok {
								violation.ConstraintConfig.Metadata.Name = name
							}
							var annotationsInterface interface{} = metadata["annotations"]
							if annotations, ok := annotationsInterface.(map[string]interface{}); ok {
								violation.ConstraintConfig.Metadata.Annotations = annotations
							}
						}

						var specInterface interface{} = constraintConfig["spec"]
						if spec, ok := specInterface.(map[string]interface{}); ok {
							var severityInterface interface{} = spec["severity"]
							if severity, ok := severityInterface.(string); ok {
								violation.ConstraintConfig.Spec.Severity = severity
							}
							var matchInterface interface{} = spec["match"]
							if match, ok := matchInterface.(map[string]interface{}); ok {
								violation.ConstraintConfig.Spec.Match = match
							}
							var parametersInterface interface{} = spec["parameters"]
							if parameters, ok := parametersInterface.(map[string]interface{}); ok {
								violation.ConstraintConfig.Spec.Parameters = parameters
							}
						}
					}
				}
				violations = append(violations, violation)
			}
		}
	}
	return violations, errorType, errorDetail
}
