// Copyright 2024 Google LLC
//
// Licensed under the Apache License, Version 2.0 (the 'License');
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an 'AS IS' BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

/*
Package monitor in the monitor go module is a function based on the function framework to check asset compliance

# Triggered by

# A cloud event containing one asset config (the feed message) and one rule

# Output

- Cloud Event on PubSub violation topic.

- Cloud Event on  complianceStatus topic.

# Cardinality

- When compliant: one-one, only the compliance state, no violations.

- When not compliant: one-few, 1 compliance state + n violations.

# Automatic retrying

Yes.
*/
package monitor
